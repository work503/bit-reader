#include <stdio.h>
#include "tests.h"
#include "read_bits.h"

void vTest(const void *vpAppOutput, const void *vpTestOutput, const DATA_TYPE_T enOutputType, const UNSIGNED32 u32OutputArraySize)
{
    /* Comparing bytes of the actual output and expected output, if fail print position of a byte, actual and expected value*/

    if ((vpAppOutput != NULL) && (vpTestOutput != NULL))
    {
        BOOL_T enSuccess = enTRUE;
        UNSIGNED32 u32Idx;
        for (u32Idx = 0U; u32Idx < (((UNSIGNED32)enOutputType * u32OutputArraySize) / 8U); u32Idx++)
        {
            if (((const UNSIGNED8 *)vpAppOutput)[u32Idx] != ((const UNSIGNED8 *)vpTestOutput)[u32Idx])
            {
                enSuccess = enFALSE;
                break;
            }
        }
        if (enSuccess)
        {
            printf("Assertion successful!\n");
        }
        else
        {
            printf("Assertion failed, Byte Position: %lu --- Expected: %x, Got: %x\n",
                   u32Idx, ((const UNSIGNED8 *)vpTestOutput)[u32Idx], ((const UNSIGNED8 *)vpAppOutput)[u32Idx]);
        }
    }
}

void vTests_u16Tou8()
{
    UNSIGNED16 u16In1 = 0xBA98U;
    UNSIGNED8 u8TestOut1 = 0x13U;
    UNSIGNED8 u8Out1 = 0U;
    vGetBits(&u16In1, 1U, enU16, &u8Out1, 3U, 5U);
    vTest(&u8Out1, &u8TestOut1, enU8, (UNSIGNED32)1u);
}
void vTests_2xu16Tou32()
{
    UNSIGNED16 u16In2[] = {0x9876U, 0xDCBAU};
    UNSIGNED32 u32TestOut2 = 0x1CBA9876U;
    UNSIGNED32 u32Out2 = 0U;
    vGetBits(&u16In2[0], 2U, enU16, &u32Out2, 0U, 30U);
    vTest(&u32Out2, &u32TestOut2, enU8, (UNSIGNED32)2U);
}
void vTests_u32To2xu16()
{
    UNSIGNED32 u32In3 = 0xFEDCBA98U;
    UNSIGNED8 u8TestOut3[] = {0xB7U, 0x0FU};
    UNSIGNED8 u8Out3[] = {0U, 0U};
    vGetBits(&u32In3, 2U, enU16, &u8Out3[0], 18U, 12U);
    vTest(&u8Out3[0], &u8TestOut3[0], enU8, (UNSIGNED32)2U);
}
void vTests_3xu8To2xu16()
{
    UNSIGNED8 u8In4[] = {0x98U, 0xBAU, 0xDCU};
    UNSIGNED16 u16TestOut4[] = {0x5D4CU, 0xEU};
    UNSIGNED16 u16Out4[] = {0U, 0U};
    vGetBits(u8In4, 3U, enU8, &u16Out4[0], 1U, 20U);
    vTest(&u16Out4[0], &u16TestOut4[0], enU16, (UNSIGNED32)2U);
}
