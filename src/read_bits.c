#include <stdio.h>
#include "read_bits.h"

void vGetBits(void *vpInput, const UNSIGNED16 u16InputArraySize, const DATA_TYPE_T enInputDataType, void *vpOutput, const UNSIGNED16 u16StartBit, const UNSIGNED16 u16BitLen)
{

    if ((vpInput != NULL) && (vpOutput != NULL))
    {
        UNSIGNED8 *pu8InputData = (UNSIGNED8 *)vpInput;
        UNSIGNED8 *pu8Tmp0 = (UNSIGNED8 *)vpOutput;
        UNSIGNED8 u8BitShift = (UNSIGNED8)(u16StartBit % 8U);
        UNSIGNED16 u16Idx = u16StartBit / 8U;
        UNSIGNED16 u16CurrentLength = 0U;
        UNSIGNED8 u8LeftHalf = 0U;
        UNSIGNED8 u8RightHalf = 0U;

        /*
            Masking 2 adjacent bytes based on a start position in a byte,
            which creates 2 halves of a new byte to be written to output
        */
        while (u16CurrentLength < u16BitLen)
        {
            if ((u16Idx + 1U) >= ((u16InputArraySize * (UNSIGNED16)enInputDataType) / 8U))
            {
                u8LeftHalf = 0U;
            }
            else
            {
                u8LeftHalf = (pu8InputData[u16Idx + 1U] & ((UNSIGNED8)0xFFU >> (8U - u8BitShift))) << (8U - u8BitShift);
            }
            u8RightHalf = ((pu8InputData[u16Idx] & ((UNSIGNED8)0xFFU << u8BitShift)) >> u8BitShift);
            *pu8Tmp0 = (u8LeftHalf | u8RightHalf);
            u16CurrentLength += 8U;
            u16Idx++;
            if (u16CurrentLength < u16BitLen)
                pu8Tmp0++;
            else
            {
                /*empty*/
            }
        }
        *pu8Tmp0 &= (UNSIGNED8)(0xFFU >> (u16CurrentLength - u16BitLen));
    }
}
