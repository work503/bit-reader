#ifndef TEST_H_
#define TEST_H_
#include "data_types.h"

/**
 * Test function comparing actual with expected output
 * @param vpAppOutput Constant void pointer to Output of a tested function
 * @param vpTestOutput Constant void pointer to Expected Output of a tested function
 * @param enOutputType Enum of a Data type used for output
 * @param u32OutputArraySize Unsigned int Ouput array size      
 */
void vTest(const void * vpAppOutput, const void * vpTestOutput, const DATA_TYPE_T enOutputType, const UNSIGNED32 u32OutputArraySize);
/**
 * Test function having unsigned short on input and an unsigned char on output
 */
void vTests_u16Tou8(void);
/**
 * Test function having array of 2 unsigned shorts on input and an unsigned int on output
 */
void vTests_2xu16Tou32(void);
/**
 * Test function having an unsigned in on input and an array of 2 unsigned shorts on output
 */
void vTests_u32To2xu16(void);
/**
 * Test function having array of 3 unsigned chars on input and an array of 2 unsigned shorts on output
 */
void vTests_3xu8To2xu16(void);

#endif /*TEST_H_*/
