#ifndef DATA_TYPES_H_
#define DATA_TYPES_H_

/* prefix for objects of the defined type */
typedef unsigned char UNSIGNED8;       /* u8 */
typedef unsigned short UNSIGNED16;     /* u16 */
typedef unsigned long UNSIGNED32;      /* u32 */
typedef unsigned long long UNSIGNED64; /* u64 */
typedef signed char SIGNED8;           /* s8 */
typedef signed short SIGNED16;         /* s16 */
typedef signed long SIGNED32;          /* s32 */
typedef long long SIGNED64;            /* s64 */

typedef enum
{
    enFALSE = 0x00,
    enTRUE = 0xFF
} BOOL_T;                              /* b */

typedef enum
{
    enU8 = 8,
    enU16 = 16,
    enU32 = 32
} DATA_TYPE_T;


#endif /* DATA_TYPES_H_ */
