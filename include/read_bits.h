#ifndef READ_BITS_H_
#define READ_BITS_H_
#include "data_types.h"

/**
 * Function reading sequence of bits based on position of a start bit and length of a sequence to be read
 * @param vpInput           Constant void pointer, Input data
 * @param u16InputArraySize Unsigned short, Input Array Size
 * @param enInputDataType   Constant enum, Datatype of input
 * @param vpOutput          Void pointer, Output sequence
 * @param u16StartBit       Unsigned short, Position of a first bit in the sequence
 * @param u16BitLen         Unsigned short, Length of output sequence
 */
void vGetBits(void *vpInput, const UNSIGNED16 u16InputArraySize, const DATA_TYPE_T enInputDataType, void *vpOutput, const UNSIGNED16 u16StartBit, const UNSIGNED16 u16BitLen);

#endif /*READ_BITS_H_*/
