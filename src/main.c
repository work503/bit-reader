#include "read_bits.h"
#include "tests.h"
#include "data_types.h"

int main()
{
    vTests_u16Tou8();
    vTests_u32To2xu16();
    vTests_2xu16Tou32();
    vTests_3xu8To2xu16();

    return 0;
}
