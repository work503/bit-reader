MKDIR   := mkdir
RMDIR   := rm -rf
CC      := gcc
BIN     := ./bin
OBJ     := ./obj
INCLUDE := ./include
SRC     := ./src
SRCS    := $(wildcard $(SRC)/*.c)
OBJS    := $(patsubst $(SRC)/%.c,$(OBJ)/%.o,$(SRCS))
EXE     := $(BIN)/main
CFLAGS  := -g -Wall -pedantic -Wextra -std=c99 -I$(INCLUDE) -O2

all: $(EXE)

$(EXE): $(OBJS) | $(BIN)
	$(CC) $(LDFLAGS) $^ -o $@

$(OBJ)/%.o: $(SRC)/%.c | $(OBJ)
	$(CC) $(CFLAGS) -c $< -o $@

$(OBJS): $(INCLUDE)/*.h

$(BIN) $(OBJ):
	$(MKDIR) $@

run: $(EXE)
	$<

clean:  
	$(RMDIR) $(OBJ) $(BIN)

